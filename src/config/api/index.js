import Config from 'react-native-config';
const ApiConfig = {
  WEATHER_BIT_BASE_URL: Config.WEATHERBIT_URL,
  CONTRIES_NOW_BASE_URL: Config.CONTRIESNOW_URL,
  COUNTRIES_POSITION: '/countries/positions',
  HISTORY_DAILY: '/history/daily',
};

export default ApiConfig;
