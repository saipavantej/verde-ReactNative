const FONTS_APP = {
  PlayfairDisplay: {
    Medium: 'PlayfairDisplay-Medium',
    Regular: 'PlayfairDisplay-Regular',
    Bold: 'PlayfairDisplay-Bold',
  },
};

const FONTS = FONTS_APP;

export default FONTS;
