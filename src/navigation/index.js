import React, {memo} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '@screens/home';
import Charts from 'screens/charts';
import ROUTES from '@config/routes';
import AppHeader from '@components/AppHeader/AppHeader';
const Stack = createNativeStackNavigator();

const RootNavigation = memo(() => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={ROUTES.Home}
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={ROUTES.Charts}
        component={Charts}
        options={{
          header: () => {
            return <AppHeader title={'Carts Screen'} />;
          },
        }}
      />
    </Stack.Navigator>
  );
});

export default RootNavigation;
