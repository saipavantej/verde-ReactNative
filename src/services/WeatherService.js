import {WeatherService} from '@utils/AppClient';
import ApiConfig from '@config/api';

const DailyWeatherHistory = payload => {
  return WeatherService.get(ApiConfig.HISTORY_DAILY, {
    params: payload,
  }).catch(error => {
    return error;
  });
};

export {DailyWeatherHistory};
