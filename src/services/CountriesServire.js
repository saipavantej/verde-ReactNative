import {countriesService} from '@utils/AppClient';
import ApiConfig from '@config/api';

const CountriesPositions = payload => {
  return countriesService
    .get(ApiConfig.COUNTRIES_POSITION, {
      params: payload,
    })
    .catch(error => {
      return error;
    });
};

export {CountriesPositions};
