import React, {useEffect, useState} from 'react';
import {Dimensions} from 'react-native';
export default function CheckOrientation() {
  const [screenData, setScreenData] = useState(Dimensions.get('window'));

  useEffect(() => {
    const onChange = result => {
      setScreenData(result.screen);
    };

    const subscription = Dimensions.addEventListener('change', onChange);

    return () => subscription.remove();
  });

  return {
    ...screenData,
    isLandscape: screenData.width > screenData.height,
    isPotrait: screenData.width < screenData.height,
  };
}
