import axios from 'axios';
import ApiConfig from '@config/api';
const WeatherService = axios.create({
  baseURL: ApiConfig.WEATHER_BIT_BASE_URL,
  responseType: 'json',
  withCredentials: true,
});
const countriesService = axios.create({
  baseURL: ApiConfig.CONTRIES_NOW_BASE_URL,
  responseType: 'json',
  withCredentials: true,
});
export {WeatherService, countriesService};
