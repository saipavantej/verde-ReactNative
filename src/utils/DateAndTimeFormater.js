import moment from 'moment';
const TodaysDate = new Date();
const dToMD = date => {
  return moment(date).format('MMM DD');
};

const endDate = () => {
  return moment(TodaysDate).format('YYYY-MM-DD');
};
const startDate = () => {
  return moment(TodaysDate).add(-7, 'days').format('YYYY-MM-DD');
};
export {dToMD, startDate, endDate};
