import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  divider: {
    padding: 10,
  },
});
export default styles;
