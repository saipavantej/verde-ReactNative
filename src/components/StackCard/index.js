import {View, Text, TouchableOpacity} from 'react-native';
import VectorIcons from '@components/VectorIcons';
import React from 'react';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
const StackCard = ({
  index,
  length,
  containerStyle,
  iconSize,
  route,
  lable,
  iconName,
  iconColor,
  iconPackage,
  dividerStyle,
  routeParams,
}) => {
  const navigation = useNavigation();
  return (
    <>
      <TouchableOpacity
        style={[styles.card, containerStyle]}
        onPress={() => {
          navigation.navigate(route, routeParams);
        }}>
        <Text>{lable}</Text>
        <Text>
          <VectorIcons
            iconPackage={iconPackage}
            iconName={iconName}
            iconSize={iconSize}
            iconColor={iconColor}
          />
        </Text>
      </TouchableOpacity>
      {index !== length && <View style={[styles.divider, dividerStyle]} />}
    </>
  );
};

export default StackCard;
