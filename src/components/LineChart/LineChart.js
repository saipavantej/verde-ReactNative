import React from 'react';
import {
  VictoryAxis,
  VictoryChart,
  VictoryGroup,
  VictoryLabel,
  VictoryLine,
  VictoryScatter,
  VictoryTheme,
} from 'victory-native';

const LineChart = props => {
  return (
    <VictoryChart
      width={props.screenWidth}
      theme={VictoryTheme.material}
      domainPadding={{x: 50}}>
      <VictoryGroup data={props.lineChartData1} color="blue">
        <VictoryLine
          style={{data: {stroke: 'blue', strokeWidth: 1}}}
          animate={{
            onExit: {
              duration: 5000,
              before: () => ({
                _y: 0,
                fill: 'orange',
              }),
            },
          }}
        />
        <VictoryScatter size={6} symbol="circle" />
      </VictoryGroup>
      <VictoryGroup data={props.lineChartData2} color="#999990">
        <VictoryLine
          style={{
            data: {stroke: '#999990', strokeWidth: 1},
          }}
          animate={{
            onExit: {
              duration: 5000,
              before: () => ({
                _y: 0,
                fill: 'orange',
              }),
            },
          }}
        />
        <VictoryScatter size={6} symbol="circle" />
      </VictoryGroup>
      <VictoryAxis
        dependentAxis
        style={{
          grid: {stroke: '#818e99', strokeWidth: 1},
        }}
        axisLabelComponent={
          <VictoryLabel
            dy={-30}
            backgroundStyle={{fill: 'blue', opacity: 0.2}}
            backgroundPadding={5}
          />
        }
        backgroundStyle={{fill: 'pink'}}
        backgroundPadding={5}
        label={props.yLable}
      />
      <VictoryAxis
        style={{
          grid: {stroke: '#818e99', strokeWidth: 0},
          tickLabels: {angle: 45},
        }}
        axisLabelComponent={
          <VictoryLabel
            backgroundStyle={{fill: 'blue', opacity: 0.2}}
            backgroundPadding={5}
            dy={25}
          />
        }
        label={props.xLable}
      />
    </VictoryChart>
  );
};

export default LineChart;
