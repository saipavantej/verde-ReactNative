import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Foundation from 'react-native-vector-icons/Foundation';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import Zocial from 'react-native-vector-icons/Zocial';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const VectorIcons = ({iconPackage, iconSize, iconName, iconColor}) => {
  const Icon = ({iconType, size, name, color}) => {
    switch (iconType) {
      case 'SimpleLineIcons':
        return <SimpleLineIcons name={name} color={color} size={size} />;
      case 'MaterialIcons':
        return <MaterialIcons name={name} color={color} size={size} />;
      case 'FontAwesome':
        return <FontAwesome name={name} color={color} size={size} />;
      case 'Foundation':
        return <Foundation name={name} color={color} size={size} />;
      case 'EvilIcons':
        return <EvilIcons name={name} color={color} size={size} />;
      case 'Octicons':
        return <Octicons name={name} color={color} size={size} />;
      case 'Ionicons':
        return <Ionicons name={name} color={color} size={size} />;
      case 'Feather':
        return <Feather name={name} color={color} size={size} />;
      case 'Entypo':
        return <Entypo name={name} color={color} size={size} />;
      case 'Zocial':
        return <Zocial name={name} color={color} size={size} />;
      case 'AntDesign':
        return <AntDesign name={name} color={color} size={size} />;
      case 'Fontisto':
        return <Fontisto name={name} color={color} size={size} />;
      case 'FontAwesome5':
        return <FontAwesome5 name={name} color={color} size={size} />;
      default:
        return <MaterialCommunityIcons name={name} color={color} size={size} />;
    }
  };
  return (
    <Icon
      iconType={iconPackage}
      size={iconSize}
      name={iconName}
      color={iconColor}
    />
  );
};

export default VectorIcons;
