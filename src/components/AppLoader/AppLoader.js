import {ActivityIndicator, Text, View} from 'react-native';
import React from 'react';

const AppLoader = ({
  size,
  text,
  color,
  outerContainerStyle,
  innerContainerStyle,
}) => {
  return (
    <View
      style={[
        {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        },
        outerContainerStyle,
      ]}>
      <View
        style={[
          {
            justifyContent: 'center',
            alignItems: 'center',
            padding: 5,
          },
          innerContainerStyle,
        ]}>
        <ActivityIndicator color={color} size={size} />
        {text && (
          <View style={{paddingTop: 10}}>
            <Text>{text}</Text>
          </View>
        )}
      </View>
    </View>
  );
};

export default AppLoader;
