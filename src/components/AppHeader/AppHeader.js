import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import VectorIcons from '@components/VectorIcons';
const AppHeader = ({title, subTitle, containerStyle}) => {
  const navigation = useNavigation();
  const _backHandler = () => {
    navigation.goBack();
  };
  return (
    <View
      style={[
        {
          paddingHorizontal: 20,
          paddingTop: 20,
          paddingBottom: 10,
          backgroundColor: '#F9FBFF',
        },
        containerStyle,
      ]}>
      <SafeAreaView />
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          style={{height: 20, width: 20, alignSelf: 'center'}}
          onPress={_backHandler}>
          <VectorIcons
            iconColor={'#000000'}
            iconName={'arrow-back-outline'}
            iconSize={20}
            iconPackage={'Ionicons'}
          />
        </TouchableOpacity>
        <View style={{alignSelf: 'center'}}>
          <Text
            style={
              {
                // fontFamily: fonts.Gordita.Medium,
                // color: '#182232',
                // fontSize: sizes.h3,
              }
            }>
            {title}
          </Text>
        </View>
      </View>
      {subTitle && (
        <View style={{marginHorizontal: 30}}>
          <Text
            style={
              {
                // color: colors.waterloo,
                // fontFamily: fonts.Gordita.Regular,
                // fontSize: sizes.h6,
              }
            }>
            {subTitle}
          </Text>
        </View>
      )}
    </View>
  );
};

export default AppHeader;
