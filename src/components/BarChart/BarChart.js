import React from 'react';
import {
  VictoryAxis,
  VictoryBar,
  VictoryChart,
  VictoryLabel,
  VictoryTheme,
} from 'victory-native';

const BarChart = props => {
  return (
    <VictoryChart
      width={props.screenWidth}
      theme={VictoryTheme.material}
      domainPadding={{x: 50}}>
      <VictoryBar
        data={props.barChartData}
        style={{data: {fill: '#c43a31'}}}
        x={props.xAxis}
        y={props.yAxis}
        barRatio={0.5}
        animate={{
          onExit: {
            duration: 5000,
            before: () => ({
              _y: 0,
              fill: 'orange',
            }),
          },
        }}
      />
      <VictoryAxis
        dependentAxis
        style={{
          grid: {stroke: '#818e99', strokeWidth: 1},
        }}
        axisLabelComponent={
          <VictoryLabel
            backgroundStyle={{fill: 'blue', opacity: 0.2}}
            backgroundPadding={5}
            dy={-30}
          />
        }
        label={props.yLable}
      />
      <VictoryAxis
        style={{
          grid: {stroke: '#818e99', strokeWidth: 0},
          tickLabels: {angle: 45},
        }}
        axisLabelComponent={
          <VictoryLabel
            backgroundStyle={{fill: 'blue', opacity: 0.2}}
            backgroundPadding={5}
            dy={25}
          />
        }
        label={props.xLable}
      />
    </VictoryChart>
  );
};

export default BarChart;
