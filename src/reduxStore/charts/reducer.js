import {
  GET_WEATHER_REPORT_SUCCESS,
  GET_WEATHER_REPORT_FAILED,
  LOADING_START,
  LOADING_END,
} from 'reduxStore/charts/type';
const initialState = {
  barChartData: [],
  lineChartData1: [],
  lineChartData2: [],
  errorMessage: '',
  loading: false,
};

const chartsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_WEATHER_REPORT_SUCCESS:
      return {
        ...state,
        barChartData: action.payload.barChartData,
        lineChartData1: action.payload.lineChartData1,
        lineChartData2: action.payload.lineChartData2,
        errorMessage: '',
      };
    case GET_WEATHER_REPORT_FAILED:
      return {
        ...state,
        barChartData: [],
        lineChartData1: [],
        lineChartData2: [],
        errorMessage: JSON.stringify(action.payload),
      };
    case LOADING_START:
      return {
        ...state,
        loading: true,
        errorMessage: '',
      };
    case LOADING_END:
      return {
        ...state,
        loading: false,
      };
    default:
      return {
        ...state,
      };
  }
};
export default chartsReducer;
