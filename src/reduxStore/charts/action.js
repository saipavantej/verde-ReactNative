import {
  GET_WEATHER_REPORT_SUCCESS,
  GET_WEATHER_REPORT_FAILED,
  LOADING_START,
  LOADING_END,
} from 'reduxStore/charts/type';
import {DailyWeatherHistory} from '@services/WeatherService';
import {dToMD} from '@utils/DateAndTimeFormater';
export const getDailyWeatherHistory = queryRequest => async dispatch => {
  dispatch({
    type: LOADING_START,
  });
  DailyWeatherHistory(queryRequest)
    .then(response => {
      try {
        const payload = {};
        var barChartData = response.data.data.map(item => {
          return {
            date: dToMD(item.datetime),
            temperature: item.min_temp,
            clouds: item.clouds,
          };
        });
        var lineChartData1 = response.data.data.map(item => {
          return {
            x: dToMD(item.datetime),
            y: item.min_temp,
          };
        });
        var lineChartData2 = response.data.data.map(item => {
          return {
            x: dToMD(item.datetime),
            y: item.clouds,
          };
        });
        Object.assign(payload, {
          barChartData,
          lineChartData1,
          lineChartData2,
        });
        dispatch({
          type: GET_WEATHER_REPORT_SUCCESS,
          payload: payload,
        });
        dispatch({
          type: LOADING_END,
        });
      } catch {
        dispatch({
          type: GET_WEATHER_REPORT_FAILED,
          payload: response,
        });
        dispatch({
          type: LOADING_END,
        });
      }
    })
    .catch(error => {
      dispatch({
        type: GET_WEATHER_REPORT_FAILED,
        payload: error,
      });
      dispatch({
        type: LOADING_END,
      });
    });
};
