import homeReducer from '@reduxStore/home/reducer';
import chartsReducer from '@reduxStore/charts/reducer';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({
  homeReducer,
  chartsReducer,
});
export default rootReducer;
