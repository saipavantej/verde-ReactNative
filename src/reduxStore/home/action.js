import {
  GET_COUNTRIES_LIST_SUCCESS,
  GET_COUNTRIES_LIST_FAILED,
  LOADING_START,
  LOADING_END,
} from 'reduxStore/home/type';
import {CountriesPositions} from 'services/CountriesServire';
export const getCountriesList = queryRequest => async dispatch => {
  dispatch({
    type: LOADING_START,
  });
  CountriesPositions(queryRequest).then(response => {
    try {
      if (response.status === 200) {
        dispatch({
          type: GET_COUNTRIES_LIST_SUCCESS,
          payload: response.data.data,
        });
        dispatch({
          type: LOADING_END,
        });
      } else {
        dispatch({
          type: GET_COUNTRIES_LIST_FAILED,
          payload: response,
        });
        dispatch({
          type: LOADING_END,
        });
      }
    } catch (error) {
      dispatch({
        type: GET_COUNTRIES_LIST_FAILED,
        payload: error,
      });
      dispatch({
        type: LOADING_END,
      });
    }
  });
};
