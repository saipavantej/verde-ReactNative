import {
  GET_COUNTRIES_LIST_SUCCESS,
  GET_COUNTRIES_LIST_FAILED,
  LOADING_START,
  LOADING_END,
} from 'reduxStore/home/type';
const initialState = {
  contriesList: [],
  errorMessage: '',
  loading: false,
};
const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_COUNTRIES_LIST_SUCCESS:
      return {
        ...state,
        contriesList: action.payload,
        errorMessage: '',
      };
    case GET_COUNTRIES_LIST_FAILED:
      return {
        ...state,
        contriesList: [],
        errorMessage: JSON.stringify(action.payload),
      };
    case LOADING_START:
      return {
        ...state,
        loading: true,
        errorMessage: '',
      };
    case LOADING_END:
      return {
        ...state,
        loading: false,
      };
    default:
      return {
        ...state,
      };
  }
};
export default homeReducer;
