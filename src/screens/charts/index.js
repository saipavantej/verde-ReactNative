import React, {useEffect, useState} from 'react';
import {View, SafeAreaView, ScrollView, Text, Button} from 'react-native';
import CheckOrientation from '@utils/CheckOrientation';
import Config from 'react-native-config';
import {connect} from 'react-redux';
import {getDailyWeatherHistory} from '@reduxStore/charts/action';
import {useFocusEffect} from '@react-navigation/native';
import BarChart from '@components/BarChart/BarChart';
import LineChart from '@components/LineChart/LineChart';
import AppLoader from '@components/AppLoader/AppLoader';
import Toast from 'react-native-toast-message';
import {endDate, startDate} from '@utils/DateAndTimeFormater';
const Charts = ({
  route,
  getDailyWeatherHistory,
  barChartData,
  lineChartData1,
  lineChartData2,
  loadingRedux,
  errorMessageRedux,
}) => {
  const screenData = CheckOrientation();
  const [screenWidth, setScreenWidth] = useState(screenData.width);
  const {lat, lon} = route.params;
  useFocusEffect(
    React.useCallback(() => {
      getChartsData();
    }, []),
  );

  useEffect(() => {
    if (errorMessageRedux !== '') {
      showToast();
    }
  }, [errorMessageRedux]);

  const showToast = () => {
    Toast.show({
      type: 'error',
      text2: errorMessageRedux,
    });
  };
  const getChartsData = () => {
    const query = {
      lat,
      lon,
      start_date: startDate(),
      end_date: endDate(),
      key: Config.API_KEY,
    };
    getDailyWeatherHistory(query);
  };
  useEffect(() => {
    screenProperties();
  }, [screenData.isLandscape]);

  const screenProperties = () => {
    setScreenWidth(screenData.width);
  };

  return (
    <View style={{flex: 1}}>
      <SafeAreaView />
      {!loadingRedux ? (
        <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
          <View style={{padding: 20}}>
            <View>
              <Text>Date wise Temperature </Text>
            </View>
            <BarChart
              screenWidth={screenWidth}
              barChartData={barChartData}
              yLable={'Temperature'}
              xLable={'Date'}
              xAxis={'date'}
              yAxis={'clouds'}
            />
            <View style={{paddingTop: 20}}>
              <Text>Date wise Temperature & Clouds</Text>
            </View>
            <LineChart
              screenWidth={screenWidth}
              lineChartData1={lineChartData1}
              lineChartData2={lineChartData2}
              yLable={'Temperature & Clouds'}
              xLable={'Date'}
            />
          </View>
        </ScrollView>
      ) : (
        <AppLoader
          outercontainerStyle={{backgroundColor: 'gray'}}
          innerContainerStyle={{
            backgroundColor: 'white',
            padding: 10,
            borderRadius: 10,
          }}
          text={'loading'}
          color={'green'}
        />
      )}

      {!errorMessageRedux == '' && (
        <View
          style={{justifyContent: 'center', alignItems: 'center', margin: 20}}>
          <View style={{padding: 10}}>
            <Text> Failed To Fetch Data </Text>
          </View>
          <Button title="retry" onPress={() => getChartsData()} />
        </View>
      )}
      <SafeAreaView />
    </View>
  );
};

const mapStateToProps = (state, oneProps) => {
  return {
    barChartData: state.chartsReducer.barChartData,
    lineChartData1: state.chartsReducer.lineChartData1,
    lineChartData2: state.chartsReducer.lineChartData2,
    errorMessageRedux: state.chartsReducer.errorMessage,
    loadingRedux: state.chartsReducer.loading,
  };
};
const mapDispatchToProps = () => {
  return {
    getDailyWeatherHistory,
  };
};
export default connect(mapStateToProps, mapDispatchToProps())(Charts);
