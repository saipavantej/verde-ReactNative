import React, {useEffect} from 'react';
import {View, SafeAreaView, FlatList, Text, Button} from 'react-native';
import ROUTES from '@config/routes';
import {getCountriesList} from '@reduxStore/home/action';
import StackCard from '@components/StackCard';
import {connect} from 'react-redux';
import Toast from 'react-native-toast-message';
import AppLoader from '@components/AppLoader/AppLoader';
const Home = ({
  contriesListRedux,
  errorMessageRedux,
  loadingRedux,
  getCountriesList,
}) => {
  useEffect(() => {
    getCountriesList();
  }, []);

  useEffect(() => {
    if (errorMessageRedux !== '') {
      showToast();
    }
  }, [errorMessageRedux]);

  const showToast = () => {
    Toast.show({
      type: 'error',
      text2: errorMessageRedux,
    });
  };
  const _Title = () => {
    return (
      <View
        style={{
          justifyContent: 'center',
          width: '100%',
          alignItems: 'center',
          padding: 20,
        }}>
        <Text>Countries List</Text>
      </View>
    );
  };
  const _List = ({item, index}) => {
    return (
      <View key={index + 1} style={{marginHorizontal: 20}}>
        <StackCard
          iconPackage="Feather"
          iconName="chevron-right"
          iconSize={30}
          index={index + 1}
          length={contriesListRedux.length}
          route={ROUTES.Charts}
          routeParams={{lat: item.lat, lon: item.long}}
          lable={item.name}
          containerStyle={{
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10,
            justifyContent: 'space-between',
            backgroundColor: 'white',
            shadowOffset: {width: -2, height: 4},
            shadowOpacity: 0.2,
            shadowRadius: 3,
            elevation: 3,
            shadowColor: 'gray',
          }}
        />
      </View>
    );
  };
  const _ListEmpty = () => {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <View style={{padding: 10}}>
          <Text> Failed To Fetch Data </Text>
        </View>
        <Button title="retry" onPress={() => getCountriesList()} />
      </View>
    );
  };
  return (
    <View style={{flex: 1}}>
      <SafeAreaView />
      {!loadingRedux ? (
        <FlatList
          bounces={false}
          showsVerticalScrollIndicator={false}
          data={contriesListRedux}
          ListHeaderComponent={_Title}
          keyExtractor={(item, index) => index}
          renderItem={_List}
          ListEmptyComponent={_ListEmpty}
          maxToRenderPerBatch={30}
        />
      ) : (
        <AppLoader
          outercontainerStyle={{backgroundColor: 'gray'}}
          innerContainerStyle={{
            backgroundColor: 'white',
            padding: 10,
            borderRadius: 10,
          }}
          text={'loading'}
          color={'green'}
        />
      )}
      <SafeAreaView />
    </View>
  );
};
const mapStateToProps = (state, oneProps) => {
  return {
    contriesListRedux: state.homeReducer.contriesList,
    errorMessageRedux: state.homeReducer.errorMessage,
    loadingRedux: state.homeReducer.loading,
  };
};
const mapDispatchToProps = () => {
  return {
    getCountriesList,
  };
};
export default connect(mapStateToProps, mapDispatchToProps())(Home);
