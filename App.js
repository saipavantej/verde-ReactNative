import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {StatusBar} from 'react-native';
import RootNavigation from '@navigation';
import {Provider} from 'react-redux';
import Toast from 'react-native-toast-message';
import {store} from '@reduxStore/store';
const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <SafeAreaProvider>
          <StatusBar backgroundColor="transparent" barStyle="dark-content" />
          <RootNavigation />
          <Toast position="bottom" bottomOffset={30} />
        </SafeAreaProvider>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
